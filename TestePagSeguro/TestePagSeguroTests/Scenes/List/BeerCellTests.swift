//
//  BeerCellTests.swift
//  TestePagSeguroTests
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

@testable import TestePagSeguro

import Quick
import Nimble

final class BeerCellSpec: QuickSpec {
  override func spec() {
    let sut = BeerCell()

    describe("given a beer cell") {
      context("when display a beer") {
        it("should display beers details") {
          let name = "Heineken"
          let abv = "5.5"
          let beer = List.DisplayedBeer(imageUrlString: "", name: name, abv: abv)
          sut.update(with: beer)

          expect(sut.nameLabel.text).to(equal(name))
          expect(sut.abvValueLabel.text).to(equal(abv))
        }
      }
    }
  }
}
