//
//  DetailCellTests.swift
//  TestePagSeguroTests
//
//  Created by Gilson Gil on 20/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

@testable import TestePagSeguro

import Quick
import Nimble

final class DetailCellSpec: QuickSpec {
  override func spec() {
    let sut = DetailCell()

    describe("given a detail cell") {
      context("when display an item") {
        it("should display item's details") {
          let title = "nome"
          let value = "Heineken"
          let item = Detail.Item(title: title, value: value)
          sut.update(with: item)

          expect(sut.titleLabel.text).to(equal(title))
          expect(sut.valueLabel.text).to(equal(value))
        }
      }
    }
  }
}
