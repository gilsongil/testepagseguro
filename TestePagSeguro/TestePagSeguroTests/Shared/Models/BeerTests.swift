//
//  BeerTests.swift
//  TestePagSeguroTests
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import Foundation

@testable import TestePagSeguro

import Quick
import Nimble

final class BeerSpec: QuickSpec {
  override func spec() {
    describe("given a valid json") {
      let bundle = Bundle(for: BeerSpec.self)
      let path = bundle.path(forResource: "Mock_Beers.json", ofType: nil)!
      let url = URL(fileURLWithPath: path)
      let json = try? Data(contentsOf: url)
      context("when deserializing") {
        it("should instantiate valid Beer objects") {
          let decoder = JSONDecoder()
          let beers = try? decoder.decode([Beer].self, from: json ?? Data())
          expect(beers?.count).to(equal(1))
          expect(beers?[0].name).to(equal("Punk IPA 2007 - 2010"))
          expect(beers?[0].abv).to(equal(6))
        }
      }
    }
  }
}
