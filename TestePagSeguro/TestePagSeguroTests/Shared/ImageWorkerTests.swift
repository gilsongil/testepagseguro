//
//  ImageWorkerTests.swift
//  TestePagSeguroTests
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import Foundation

@testable import TestePagSeguro

import Quick
import Nimble

final class ImageWorkerSpec: QuickSpec {
  override func spec() {
    let sut = ImageWorker()

    describe("given a image worker") {
      context("when they are asked for an image") {
        let imageUrl = "https://images.punkapi.com/v2/45.png"
        it("should retrieve that image from memmory cache, disk cache or network") {
          sut.image(from: imageUrl) { result in
            expect(result?.1).toNot(beNil())
          }
        }

        it("should have it on disk cache") {
          expect(sut.getFile(with: imageUrl)).toEventuallyNot(beNil(),
                                                              timeout: 10,
                                                              pollInterval: 1,
                                                              description: nil)
        }
      }
    }
  }
}
