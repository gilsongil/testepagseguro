//
//  PunkAPITests.swift
//  TestePagSeguroTests
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import Foundation

@testable import TestePagSeguro

import Quick
import Nimble

final class PunkAPISpec: QuickSpec {
  override func spec() {
    describe("given a punk api") {
      context("fetching a beer list") {
        it("should retrieve a beer list") {
          PunkAPI.getBeers(page: 1) { callback in
            let beers = try? callback()
            expect(beers).toNot(beEmpty())
          }
        }
      }
    }
  }
}
