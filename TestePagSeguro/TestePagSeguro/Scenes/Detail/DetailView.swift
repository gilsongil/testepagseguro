//
//  DetailView.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 20/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import UIKit

import Cartography

protocol DetailViewLogic: class {
  var tableView: UITableView { get }
}

final class DetailView: UIView, DetailViewLogic {
  public private(set) lazy var tableView: UITableView = {
    let tableView = UITableView()
    tableView.backgroundColor = .clear
    tableView.separatorStyle = .none
    tableView.rowHeight = UITableView.automaticDimension
    return tableView
  }()

  init() {
    super.init(frame: .zero)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {
    addSubviews()
    addConstraints()

    backgroundColor = .backgroundColor
  }

  private func addSubviews() {
    addSubview(tableView)
  }

  private func addConstraints() {
    constrain(self, tableView) { view, tableView in
      tableView.edges == view.edges
    }
  }
}
