//
//  DetailImageCell.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 20/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import UIKit

import Cartography

protocol DetailImageCellLogic: class {
  func update(with imageUrlString: String)
}

final class DetailImageCell: UITableViewCell {
  private let beerImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {
    addSubviews()
    addConstraints()

    backgroundColor = .clear
    selectionStyle = .none
  }

  private func addSubviews() {
    addSubview(beerImageView)
  }

  private func addConstraints() {
    constrain(self, beerImageView) { cell, beer in
      beer.top == cell.top + 40
      beer.left == cell.left + 20
      beer.bottom == cell.bottom - 40
      beer.right == cell.right - 20
      beer.height == 300
    }
  }
}

// MARK: - BeerCell Logic
extension DetailImageCell: DetailImageCellLogic {
  func update(with imageUrlString: String) {
    ImageWorker.shared.image(from: imageUrlString) { [weak self] result in
      guard let self = self else { return }
      DispatchQueue.main.async {
        self.beerImageView.image = result?.1
      }
    }
  }
}
