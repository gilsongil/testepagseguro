//
//  DetailCell.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 20/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import UIKit

import Cartography

protocol DetailCellLogic: class {
  var titleLabel: UILabel { get }
  var valueLabel: UILabel { get }
  func update(with item: Detail.Item)
}

final class DetailCell: UITableViewCell {
  public private(set) var titleLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .center
    label.font = UIFont.lightFont
    label.textColor = .white
    return label
  }()

  public private(set) var valueLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.font = UIFont.boldFont
    label.textColor = .white
    return label
  }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {
    addSubviews()
    addConstraints()

    backgroundColor = .clear
    selectionStyle = .none
  }

  private func addSubviews() {
    addSubview(titleLabel)
    addSubview(valueLabel)
  }

  private func addConstraints() {
    constrain(self, titleLabel, valueLabel) { cell, title, value in
      title.top == cell.top + 8
      title.left == cell.left + 20
      title.right == cell.right - 20

      value.top == title.bottom + 4
      value.left == cell.left + 20
      value.bottom == cell.bottom - 8
      value.right == cell.right - 20
    }
  }
}

// MARK: - BeerCell Logic
extension DetailCell: DetailCellLogic {
  func update(with item: Detail.Item) {
    titleLabel.text = item.title
    valueLabel.text = item.value
  }
}
