//
//  String+Detail.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 20/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import Foundation

extension String {
  struct Detail {
    static var emptyValue: String {
      return "-"
    }

    static var name: String {
      return "DETAIL_NAME_LABEL".localized
    }

    static var tagline: String {
      return "DETAIL_TAGLINE_LABEL".localized
    }

    static var abv: String {
      return "DETAIL_ABV_LABEL".localized
    }

    static var ibu: String {
      return "DETAIL_IBU_LABEL".localized
    }

    static var description: String {
      return "DETAIL_DESCRIPTION_LABEL".localized
    }
  }
}
