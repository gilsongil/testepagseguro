//
//  String+List.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import Foundation

extension String {
  struct List {
    static var beerCellABVLabel: String {
      return "LIST_BEERCELL_ABV_LABEL".localized
    }
  }
}
