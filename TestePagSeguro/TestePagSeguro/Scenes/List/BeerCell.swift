//
//  BeerCell.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import UIKit

import Cartography

protocol BeerCellLogic: class {
  func update(with beer: List.DisplayedBeer)
}

final class BeerCell: UITableViewCell {
  private let container: UIView = {
    let view = UIView()
    view.backgroundColor = .white
    return view
  }()

  public private(set) var beerImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()

  public private(set) var nameLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.font = UIFont.h1Font
    label.textColor = .backgroundColor
    label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
    label.setContentCompressionResistancePriority(.required, for: .vertical)
    return label
  }()

  private lazy var abvStackView: UIStackView = {
    let stackView = UIStackView(arrangedSubviews: [abvLabel, abvValueLabel])
    stackView.axis = .vertical
    stackView.alignment = .center
    stackView.distribution = .fillProportionally
    return stackView
  }()

  private let abvLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.lightFont
    label.textColor = .backgroundColor
    label.text = String.List.beerCellABVLabel
    return label
  }()

  public private(set) var abvValueLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.boldFont
    label.textColor = .backgroundColor
    return label
  }()

  private var imageUrl: String?

  override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    super.setHighlighted(highlighted, animated: animated)
    if highlighted {
      container.backgroundColor = .selectionColor
    } else {
      container.backgroundColor = .white
    }
  }

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {
    addSubviews()
    addConstraints()

    backgroundColor = .clear
    selectionStyle = .none
  }

  private func addSubviews() {
    addSubview(container)
    container.addSubview(beerImageView)
    container.addSubview(nameLabel)
    container.addSubview(abvStackView)
  }

  private func addConstraints() {
    constrain(self, container) { cell, container in
      container.top == cell.top + 4
      container.left == cell.left + 8
      container.bottom == cell.bottom - 4
      container.right == cell.right - 8
    }

    constrain(container, beerImageView, nameLabel, abvStackView) { container, beerImageView, nameLabel, abvStackView in
      beerImageView.top == container.top + 8
      beerImageView.left == container.left + 8
      beerImageView.bottom <= container.bottom - 8
      beerImageView.width == beerImageView.height
      beerImageView.height == 50

      nameLabel.top == container.top + 8
      nameLabel.left == beerImageView.right + 8
      nameLabel.bottom == container.bottom - 8
      nameLabel.centerY == container.centerY

      abvStackView.top >= container.top + 8
      abvStackView.left == nameLabel.right + 8
      abvStackView.bottom <= container.bottom - 8
      abvStackView.right == container.right - 8
      abvStackView.centerY == container.centerY
      abvStackView.width == 50
    }
  }
}

// MARK: - BeerCell Logic
extension BeerCell: BeerCellLogic {
  func update(with beer: List.DisplayedBeer) {
    if imageUrl != beer.imageUrlString || beerImageView.image == nil {
      beerImageView.image = #imageLiteral(resourceName: "beer-icon")
    }
    imageUrl = beer.imageUrlString
    ImageWorker.shared.image(from: beer.imageUrlString) { [weak self] result in
      guard let self = self else { return }
      DispatchQueue.main.async {
        if self.imageUrl == result?.0 {
          self.beerImageView.image = result?.1
        }
      }
    }
    nameLabel.text = beer.name
    abvValueLabel.text = beer.abv
  }
}
