//
//  ListView.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import UIKit

import Cartography

protocol ListViewLogic: class {
  var tableView: UITableView { get }
  var loadingView: UIActivityIndicatorView { get }
  var logoImageView: UIImageView { get }
  var logoConstraint: NSLayoutConstraint! { get }
}

final class ListView: UIView, ListViewLogic {
  public private(set) var logoImageView: UIImageView = {
    let image = #imageLiteral(resourceName: "beer-icon")
    let imageView = UIImageView(image: image)
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()

  public private(set) lazy var tableView: UITableView = {
    let tableView = UITableView()
    tableView.backgroundColor = .clear
    tableView.separatorStyle = .none
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 50
    return tableView
  }()

  public private(set) var loadingView: UIActivityIndicatorView = {
    let view = UIActivityIndicatorView(style: .whiteLarge)
    view.hidesWhenStopped = true
    view.startAnimating()
    return view
  }()

  public private(set) var logoConstraint: NSLayoutConstraint!

  private var statusBarHeight: CGFloat {
    return UIApplication.shared.statusBarFrame.height
  }

  init() {
    super.init(frame: .zero)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {
    addSubviews()
    addConstraints()

    backgroundColor = .backgroundColor

    layoutIfNeeded()

    let topInset = logoImageView.image?.size.height ?? 100 - safeAreaInsets.top + statusBarHeight
    tableView.contentInset = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
  }

  private func addSubviews() {
    addSubview(logoImageView)
    addSubview(loadingView)
    addSubview(tableView)
  }

  private func addConstraints() {
    constrain(self, logoImageView, loadingView, tableView) { view, logoImageView, loading, tableView in
      logoImageView.top == view.top + statusBarHeight
      logoImageView.centerX == view.centerX
      logoConstraint = logoImageView.height == self.logoImageView.image?.size.height ?? 100

      loading.center == view.center

      tableView.edges == view.edges
    }
  }
}
