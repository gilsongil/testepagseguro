//
//  PunkAPI.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import Foundation

enum PunkError: LocalizedError {
  case endReached
}

struct PunkAPI {
  private static var baseUrlString: String {
    return "https://api.punkapi.com"
  }

  public static var baseUrl: URL {
    guard let url = URL(string: baseUrlString) else { fatalError("invalid base url") }
    return url
  }

  enum Endpoints: String {
    case beers = "/v2/beers"
  }

  static func getBeers(page: Int, completion: @escaping (() throws -> [Beer]) -> Void) {
    let method = Method.get
    let path = Endpoints.beers.rawValue
    let parameters = ["per_page": 50, "page": page]
    HTTPService.request(method: method, baseUrl: baseUrl, path: path, parameters: parameters) { callback in
      do {
        let data = try callback()
        let beers = try JSONDecoder().decode([Beer].self, from: data)
        guard !beers.isEmpty else { return completion { throw PunkError.endReached } }
        completion { beers }
      } catch {
        completion { throw error }
      }
    }
  }
}
