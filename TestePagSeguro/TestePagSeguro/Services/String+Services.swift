//
//  String+Services.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import Foundation

extension String {
  struct Services {
    static var unknownError: String {
      return "SERVICES_ERROR_UNKNOWN".localized
    }
  }
}
