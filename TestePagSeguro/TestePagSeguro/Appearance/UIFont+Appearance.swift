//
//  UIFont+Appearance.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import UIKit

extension UIFont {
  static var h1Font: UIFont {
    return UIFont.boldSystemFont(ofSize: 20)
  }

  static var lightFont: UIFont {
    return UIFont.systemFont(ofSize: 14, weight: .light)
  }

  static var boldFont: UIFont {
    return UIFont.boldSystemFont(ofSize: 16)
  }
}
