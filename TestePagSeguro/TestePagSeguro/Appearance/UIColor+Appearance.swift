//
//  UIColor+Appearance.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import UIKit

extension UIColor {
  convenience init(red: CGFloat, green: CGFloat, blue: CGFloat) {
    self.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
  }

  static var backgroundColor: UIColor {
    return UIColor(red: 51, green: 77, blue: 92)
  }

  static var selectionColor: UIColor {
    return UIColor(red: 220, green: 220, blue: 220)
  }
}
