//
//  String+Shared.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import Foundation

extension String {
  struct Shared {
    static var close: String {
      return "SHARED_CLOSE".localized
    }
  }
}
