//
//  NavigationController.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import UIKit

protocol NavigationControllerLogic: class {
  func hideNavigationBar(_ hide: Bool)
}

final class NavigationController: UINavigationController {
  init() {
    super.init(navigationBarClass: NavigationBar.self, toolbarClass: nil)
    viewControllers = [ListViewController()]
    setup()
  }

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {
    navigationBar.barTintColor = .backgroundColor
  }
}

extension NavigationController: NavigationControllerLogic {
  func hideNavigationBar(_ hide: Bool) {
    if hide {
      navigationBar.shadowImage = UIImage()
      navigationBar.setBackgroundImage(UIImage(), for: .default)
    } else {
      navigationBar.shadowImage = nil
      navigationBar.setBackgroundImage(nil, for: .default)
    }
    (navigationBar as? NavigationBarLogic)?.hideLogo(hide)
  }
}
