//
//  Beer.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import Foundation

struct Beer: Decodable {
  let identifier: Int
  let name: String
  let tagline: String
  let description: String
  let imageUrlString: String
  let abv: Double
  let ibu: Double?

  enum CodingKeys: String, CodingKey {
    case identifier = "id"
    case imageUrlString = "image_url"
    case name, tagline, description, abv, ibu
  }
}
