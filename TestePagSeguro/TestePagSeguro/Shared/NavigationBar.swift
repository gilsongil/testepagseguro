//
//  NavigationBar.swift
//  TestePagSeguro
//
//  Created by Gilson Gil on 19/02/19.
//  Copyright © 2019 Gilson Gil. All rights reserved.
//

import UIKit

protocol NavigationBarLogic: class {
  func hideLogo(_ hide: Bool)
}

final class NavigationBar: UINavigationBar {
  private let logoImageView: UIImageView = {
    let image = #imageLiteral(resourceName: "beer-icon")
    let imageView = UIImageView(image: image)
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {
    addSubview(logoImageView)
    tintColor = .white
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    logoImageView.frame = bounds
  }
}

extension NavigationBar: NavigationBarLogic {
  func hideLogo(_ hide: Bool) {
    logoImageView.isHidden = hide
  }
}
